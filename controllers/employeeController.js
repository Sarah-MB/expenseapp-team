var Employee = require('../models/employee');
var models = require('../models');

// Display employee create form on GET.
exports.employee_create_get = function(req, res, next) {
        // create employee GET controller logic here 
        res.render('forms/employee_form', { title: 'Create Employee',  layout: 'layouts/detail'});
};

// Handle employee create on POST.
exports.employee_create_post = function(req, res, next) {
    
            var full_nameInput = req.body.full_name;
            var emailInput = req.body.email;
            var roleInput = req.body.role;
            var role;
            
            if(roleInput == 'Manager') {
                  roleInput = 'admin';
            } else {
                  roleInput = 'staff';
            }
        
            
     models.Employee.create({
            full_name: req.body.full_name,
            role: req.body.role_id,
            email: req.body.email
        }).then(function(employee) {
            console.log("Employee created successfully");
           // check if there was an error during post creation
            res.redirect('/expense/employees');
      });
   
};

// Display employee delete form on GET.
exports.employee_delete_get = function(req, res, next) {
       models.Employee.destroy({
            // find the employee_id to delete from database
            where: {
              id: req.params.employee_id
            }
          }).then(function(employee) {
           // If an post gets deleted successfully, we just redirect to posts list
           // no need to render a page
            res.redirect('/expense/employees');
            console.log("Employee deleted successfully");
          });
};

// Handle employee delete on POST.
exports.employee_delete_post = function(req, res, next) {
         models.Employee.destroy({
            // find the author_id to delete from database
            where: {
              id: req.params.employee_id
            }
          }).then(function(employee) {
           // If an employee gets deleted successfully, we just redirect to posts list
           // no need to render a page
            res.redirect('/expense/employees');
            console.log("Employee deleted successfully");
          });
};

// Display employee update form on GET.
exports.employee_update_get = function(req, res, next) {
         // Find the post you want to update
        console.log("ID is " + req.params.employee_id);
        models.Employee.findById(
                req.params.employee_id
        ).then(function(employee) {
               // renders a post form
               res.render('forms/employee_form', { title: 'Update Employee', employee: employee, layout: 'layouts/detail'});
               console.log("Employee update get successful");
          });
};

// Handle post update on POST.
exports.employee_update_post = function(req, res, next) {
       console.log("ID is " + req.params.employee_id);
        models.Employee.update(
        // Values to update
            {
                    full_name: req.body.full_name,
                    role: req.body.role_id,
                    email: req.body.email
            },
          { // Clause
                where: 
                {
                    id: req.params.employee_id
                }
            }
        //   returning: true, where: {id: req.params.post_id} 
         ).then(function() { 
                // If an employee gets updated successfully, we just redirect to posts list
                // no need to render a page
                res.redirect("/expense/employees");  
                console.log("Employee updated successfully");
          });
};

// Display list of all employees.
exports.employee_list = function(req, res, next) {
          // controller logic to display all posts
        models.Employee.findAll(
        ).then(function(employees) {
        // renders a post list page
        console.log("rendering employee list");
        res.render('pages/employee_list', { title: 'Employee List', employees: employees, layout: 'layouts/list'} );
        console.log("Employees list renders successfully");
        });
};

// Display detail page for a specific author.
exports.employee_detail = async function(req, res, next) {
    const categories = await models.Category.findAll();
     // find a post by the primary key Pk
        models.Employee.findById(
                req.params.employee_id, {
                    include: [
                    {
                      model: models.Expense
                    }
                         ]
                }
            
        ).then(function(employee) {
        // renders an inividual post details page
        res.render('pages/employee_detail', { title: 'Employee Details', categories: categories, employee: employee, layout: 'layouts/detail'} );
        console.log("Employee detials renders successfully");
        });
};


 