var Expense = require('../models/expense');
var models = require('../models');

exports.expense_create_get = async function(req, res, next) {
        // renders a post form
        const categories = await models.Category.findAll();
        res.render('forms/expense_form', { title: 'Create Expense', categories: categories,  layout: 'layouts/detail'});
        console.log("Expense form renders successfully")
};
// Display expense create form on GET.
// exports.expense_create_get = function(req, res, next) {
//         // renders a post form
//         res.render('forms/expense_form', { title: 'Create Expense', layout: 'layouts/detail'});
//         console.log("Expense form renders successfully");
// };

// Handle expense create on POST.
exports.expense_create_post = function(req, res, next) {
    var estatuss;
    if (req.body.amount <= 1000){
        estatuss = 'approved'
    }else{
      estatuss = 'pending' 
   }

     // create a new post based on the fields in our post model
     // I have create two fields, but it can be more for your model
     const expense = models.Expense.create({
            date: req.body.date,
            detail: req.body.detail,
            amount: req.body.amount,
            type: req.body.type,
            category: req.body.category,
            estatus: estatuss,
            EmployeeId: req.body.employee_id
            // when creating a new post
           

      }
      
      );
      
       //This was the category that was selected in the front end.  
    // I queried the database to make sure the category selected exist in DB
    //  const category = await models.Category.findById(req.body.category_id);
     
    //  // If the category selected in the front end does not exist in DB return 400 error	
    //  if (!category) {
    //       return res.status(400);
    //  }

    //  // if it exist then add the post and category into the PostCategory table.
    //  // this is what is doing the magic - for that join table add.

    //       await expense.addCategory(category);


           // check if there was an error during post creation
            res.redirect('/expense/expenses');
};

// Display expense delete form on GET.
exports.expense_delete_get = function(req, res, next) {
       models.Expense.destroy({
            // find the post_id to delete from database
            where: {
              id: req.params.expense_id
            }
          }).then(function() {
           // If an post gets deleted successfully, we just redirect to posts list
           // no need to render a page
            res.redirect('/expense/expenses');
            console.log("Post deleted successfully");
          });
};

// Handle post delete on POST.
exports.expense_delete_post = function(req, res, next) {
          models.Expense.destroy({
            // find the post_id to delete from database
            where: {
              id: req.params.expense_id
            }
          }).then(function() {
           // If an post gets deleted successfully, we just redirect to posts list
           // no need to render a page
            res.redirect('/expense/expenses');
            console.log("Expense deleted successfully");
          });

 };

// Display expense update form on GET.
exports.expense_update_get = function(req, res, next) {
        // Find the expense you want to update
        console.log("ID is " + req.params.expense_id);
        models.Expense.findById(
                req.params.expense_id
        ).then(function(expense) {
               // renders a expense form
               res.render('forms/expense_form', { title: 'Update Expense', expense: expense, layout: 'layouts/detail'});
               console.log("Expense update get successful");
          });
        
};

// Handle expense update on POST.
exports.expense_update_post = function(req, res, next) {
        console.log("ID is " + req.params.expense_id);
        models.Expense.update(
        // Values to update
        // if (estatus = pending) {
        //    
       // }
            {
            date: req.body.date,
            detail: req.body.detail,
            amount: req.body.amount,
            type: req.body.type,
            category: req.body.category,
            estatus: 'Approved'
            },
          { // Clause
                where: 
                {
                    id: req.params.expense_id
                }
            }
        //   returning: true, where: {id: req.params.expense_id} 
         ).then(function() { 
                // If an post gets updated successfully, we just redirect to posts list
                // no need to render a page
                res.redirect("/expense/expenses");  
                console.log("Expense updated successfully");
          });
};

// Display detail page for a specific expense.
exports.expense_detail = function(req, res, next) {
        // find a post by the primary key Pk
        models.Expense.findById(
                req.params.expense_id
        ).then(function(expense) {
        // renders an inividual post details page
        res.render('pages/expense_detail', { title: 'Expense Details', expense: expense, layout: 'layouts/detail'} );
        console.log("Expense deteials renders successfully");
        });
};




// Display list of all posts.
exports.expense_list = function(req, res, next) {
        // controller logic to display all posts
        models.Expense.findAll(
        ).then(function(expenses) {
        // renders a post list page
        console.log("rendering expense list");
        res.render('pages/expense_list', { title: 'Expense List', expenses: expenses, layout: 'layouts/list'} );
        console.log("Expenses list renders successfully");
        });
        
};

// This is the expense homepage.
exports.index = function(req, res) {

      // find the count of posts in database
      models.Expense.findAndCountAll(
      ).then(function(expenseCount) {
          
       
        // find the count of employees in database
 
        // find the count of types in database
 
        // find the count of categories in database
 
        res.render('pages/index', {title: 'Homepage', expenseCount: expenseCount, layout: 'layouts/main'});
        
        // res.render('pages/index_list_sample', { title: 'Post Details', layout: 'layouts/list'});
        // res.render('pages/index_detail_sample', { page: 'Home' , title: 'Post Details', layout: 'layouts/detail'});

      })
    
    
    };


 