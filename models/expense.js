'use strict';
module.exports = (sequelize, DataTypes) => {
  var Expense = sequelize.define('Expense', {
    date: DataTypes.DATE,
    detail: DataTypes.STRING,
    amount: DataTypes.DECIMAL,
    type: DataTypes.STRING,
    category: DataTypes.STRING,
    estatus: DataTypes.STRING,
	  EmployeeId: DataTypes.INTEGER
  });
  
  // create expense association
  // a expense will have an employee
  // a field called EmployeeIdId will be created in our expense table inside the db
  // Expense.associate = async function (models) {
  //   models.Expense.belongsTo(models.Employee, {
  //     onDelete: "CASCADE",
  //     foreignKey: 'employee_id'
      
  //   });
    // models.Expense.belongsTo(models.Category,{ 
    //   as: 'categories', 
      
     Expense.associate = function (models) {   
    models.Expense.belongsToMany(models.Category,{ 
      as: 'categories', 
      through: 'ExpenseCategories',
      foreignKey: 'expense_id'
    });
        
  };
        
  //   models.Expense.hasMany(models.Type);{
  // };
  
  
  
  return Expense;
};

// Make sure you complete other models fields