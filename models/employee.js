'use strict';
module.exports = (sequelize, DataTypes) => {
  var Employee = sequelize.define('Employee', {
    full_name: DataTypes.STRING,
    role: DataTypes.STRING,
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          isEmail: true,
    }},
  });
  
   // create association between employee and expense
  // an author can have many posts
  Employee.associate = function(models) {
    models.Employee.hasMany(models.Expense);
  };
  

  return Employee;
};
 