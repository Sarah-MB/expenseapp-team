'use strict';
module.exports = (sequelize, DataTypes) => {
  var Type = sequelize.define('Type', {
    type_name: DataTypes.STRING,
  });
  
  Type.associate = function (models) {
  models.Type.belongsTo(models.Expense, {
      onDelete: "CASCADE",
      foreignKey: {
        allowNull: false
      }
    });
  };
  
  return Type;
};

// Make sure you complete other models fields